'use strict'



document.addEventListener('DOMContentLoaded', setup)

let cvs = document.querySelector('canvas')
let ctx = cvs.getContext('2d')

function setup() {
    
    setInterval(update, 100)
    document.addEventListener('keydown', changeDirection)
    document.addEventListener('keydown', click)
    board.draw()
    food.draw()
    snake.draw()

    state.draw()
}

function update() {
    
    if (state.state == "game"){
        ctx.clearRect(0, 0, cvs.width, cvs.height);
        board.draw()
        food.draw()

        snake.move()
        snake.draw()
        state.draw()
    }
    
}

function click() {
    if (state.state == "start"){
        state.state = "game"
        snake.direction = "right"
    }
    if (state.state == "end"){
        state.state = "start"
        snake.bodyArray = [[2, 0],[1, 0],[0, 0]]
        food.x = 12
        food.y = 12
        ctx.clearRect(0, 0, cvs.width, cvs.height);
        board.draw()
        food.draw()

        
        snake.draw()
        state.draw()
    }
}

let board = {
    cellSize : 30,
    draw : function () {
        ctx.fillStyle = "#000000"
        ctx.fillRect(0,0,cvs.width,cvs.height)
        for (let i = 0; i < this.cellSize; i++) {
            for (let j = 0; j < this.cellSize; j++) {
                ctx.fillStyle = "#808080"
                ctx.fillRect(i*this.cellSize+1,j*this.cellSize+1,this.cellSize - 2,this.cellSize -2)
            }
            
        }
    }
}

let state = {
    //start game end
    state : "start",
    draw : function () {
        if (this.state == "start"){
            ctx.fillStyle = "#ffffff"
            ctx.font = "30px Arial";
            ctx.fillText("Welcome to snake, press any key to play", 28, 200);
        } else if(this.state == "end"){
            ctx.fillStyle = "#ffffff"
            ctx.font = "30px Arial";
            ctx.fillText("GAME OVER,", 200, 200);
            ctx.fillText("press any key to go to the start menu", 55, 250);
        }
    }
}

function changeDirection(e) {
    e.preventDefault();

    // you need to change this, instead this.bodyArray[0][1] += 1, direction = up

    if (e.key === 'w' && snake.direction != "down") {
        snake.direction = "up"
    } 
    if (e.key === 'a' && snake.direction != "right") {
        snake.direction = "left"
    } 
    if (e.key === 's' && snake.direction != "up") {
        snake.direction = "down"
    } 
    if (e.key === 'd' && snake.direction != "left") {
        snake.direction = "right"
    } 
    console.log(e.keyCode)
    console.log(snake.direction)

    /*for (let i = 1; i < this.bodyArray.length; i++) {
        this.bodyArray[i][0] = this.bodyArray[i-1][0]
        this.bodyArray[i][1] = this.bodyArray[i-1][1]
        
    }*/
}

let food = {
    x : 12,
    y : 12,
    changePos : function (){
        this.x = Math.floor(Math.random() * 20)
        this.y = Math.floor(Math.random() * 20)
    },
    draw : function(){
        ctx.fillStyle = "#FF0000"
        ctx.beginPath();
        ctx.arc(this.x*board.cellSize+15, this.y*board.cellSize+15, 10, 0, 2 * Math.PI);
        ctx.fill();
    }
}
let snake = {
    size : board.cellSize - 8,
    bodyArray : [[2, 0],[1, 0],[0, 0]],
    direction : "",
    move : function () {
        console.log(this.bodyArray)
        // you need to change this, instead this.bodyArray[0][1] += 1, direction = up
       

        
        this.bodyArray.unshift([...this.bodyArray[0]]);
        

        if (this.direction === "up") {
            this.bodyArray[0][1] -= 1
        } 
        if (this.direction === "left") {
            this.bodyArray[0][0] -= 1
        } 
        if (this.direction === "down") {
            this.bodyArray[0][1] += 1
        } 
        if (this.direction === "right") {
            this.bodyArray[0][0] += 1
        } 

        this.bodyArray.pop();

        if (food.x == this.bodyArray[0][0] && food.y == this.bodyArray[0][1]){
            food.changePos()
            this.bodyArray.push([...this.bodyArray[this.bodyArray.length - 1]])
        }

        if ((this.bodyArray[0][0] < 0 || this.bodyArray[0][1] < 0 || this.bodyArray[0][0] > 19 || this.bodyArray[0][1] > 19) ){
            state.state = "end"
            
        }

        for (let i = 1; i < this.bodyArray.length; i++) {
            if ((this.bodyArray[0][0] == this.bodyArray[i][0] && this.bodyArray[0][1] == this.bodyArray[i][1]) ){
                state.state = "end"
                
            }
            
        }
        
    },
    draw : function () {
        for (let i = 0; i < this.bodyArray.length; i++) {
            ctx.fillStyle = "#ffffff"
            ctx.fillRect(this.bodyArray[i][0]*board.cellSize + 4,this.bodyArray[i][1]*board.cellSize + 4,this.size,this.size)
            
        }
    }
}